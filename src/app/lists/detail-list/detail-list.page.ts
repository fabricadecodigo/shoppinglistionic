import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../core/service/toast.service';
import { AlertService } from '../../core/service/alert.service';
import { ListService } from './../shared/list.service';
import { ProductService } from './../../products/shared/product.service';
import { List } from '../shared/list';
import { IonReorderGroup } from '@ionic/angular';

@Component({
  selector: 'app-detail-list',
  templateUrl: './detail-list.page.html',
  styleUrls: ['./detail-list.page.scss'],
})
export class DetailListPage implements OnInit {
  showSearch: boolean = false;
  showReorder: boolean = false;
  list: List = new List();
  productsByCategory: any[] = [];

  @ViewChildren('reorder') reoderGroup: QueryList<IonReorderGroup>;

  constructor(private route: ActivatedRoute, private toast: ToastService, private alert: AlertService,
    private listService: ListService, private productService: ProductService) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      await this.loasdList(parseInt(idParam));
      this.loadProducts();
    }
  }

  async loasdList(id: number) {
    this.list = await this.listService.getById(id);
  }

  async loadProducts() {
    const products = await this.productService.getAll(this.list.id);
    this.fillProductsList(products);
  }

  fillProductsList(products: any[]) {
    this.productsByCategory = [];
    
    if (products.length > 0) {
      let current: any = null;

      products.forEach(product => {
        if (current == null || (product.categoryName != current.categoryName)) {
          current = {};
          current.categoryName = product.categoryName;
          current.categoryId = product.categoryId;
          current.products = [];

          this.productsByCategory.push(current);
        }

        const item: any = {}
        item.id = product.id;
        item.name = product.name;
        item.amount = product.amount;
        item.sequence = product.sequence;
        item.purchased = product.purchased;
        current.products.push(item);
      });
    }
  }

  searchButtonClick() {
    this.showSearch = true;
  }

  doSerchBarCancel() {
    this.showSearch = false;
    
  }

  async doSerchBarChange($event: any) {
    const value = $event.target.value;
    if (value && value.length >= 2) {
      const products = await this.productService.filter(this.list.id, value);
      this.fillProductsList(products);
    }
  }

  delete(category: any, product: any) {
    this.alert.showConfirmDelete(product.name, () => this.executeDelete(category, product));
  }

  private async executeDelete(category: any, product: any) {
    try {
      await this.productService.delete(product)

      const index = category.products.indexOf(product);
      category.products.splice(index, 1);

      if (category.products.length == 0) {
        const index = this.productsByCategory.indexOf(category)
        this.productsByCategory.splice(index, 1);
      }

      this.toast.showSuccess('Produto excluído com sucesso.');
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar excluir o Produto.');
    }
  }

  onCheckBoxChange(product: any) {
    product.purchased = !product.purchased;
    this.productService.setProductPurchased(product.id, product.purchased);
  }

  reorderButtonClick() {
    this.showReorder = !this.showReorder;
    this.reoderGroup.forEach((reorder: IonReorderGroup) => {
      reorder.disabled = !reorder.disabled;
    });
  }

  doReorder($event: any, category: any) {
    const products: any[] = category.products;
    const productToReorder = products[$event.detail.from];

    products.splice($event.detail.from, 1);
    products.splice($event.detail.to, 0, productToReorder);

    let sequence = 1;
    products.forEach((p: any) => {
      p.sequence = sequence;
      sequence++;
    });

    $event.detail.complete();

    this.productService.updateProductsSequence(products);
  }
}
