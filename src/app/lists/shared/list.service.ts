import { Injectable } from '@angular/core';
import { DatabaseService } from './../../core/service/database.service';
import { List } from './list';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private db: DatabaseService) { }

  save(list: List) {
    if (list.id > 0) {
      return this.update(list);
    } else {
      return this.insert(list);
    }
  }

  private insert(list: List) {
    const sql = 'insert into lists (name) values (?)'
    const data = [list.name];

    return this.db.executeSQL(sql, data);
  }

  private update(list: List) {
    const sql = 'update lists set name = ? where id = ?'
    const data = [list.name, list.id];

    return this.db.executeSQL(sql, data);
  }

  delete(list: List) {
    const sql = 'delete from lists where id = ?'
    const data = [list.id];

    return this.db.executeSQL(sql, data);
  }

  private fillLists(rows: any) {
    const lists: List[] = [];

    for (let i = 0; i < rows.length; i++) {
      const item = rows.item(i);
      const list = new List();
      list.id = item.id;
      list.name = item.name;
      lists.push(list);
    }

    return lists;
  }

  async getAll() {
    const sql = 'select * from lists';
    const result = await this.db.executeSQL(sql);
    const lists = this.fillLists(result.rows);
    return lists;
  }

  async filter(text: string) {
    const sql = 'select * from lists where name like ?';
    const data = [`%${text}%`];
    const result = await this.db.executeSQL(sql, data);
    const list = this.fillLists(result.rows);
    return list;
  }

  async getById(id: number) {
    const sql = 'select * from lists where id = ?';
    const data = [id];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const list = new List();
    if (rows && rows.length > 0) {
      // get first item
      const item = rows.item(0);
      list.id = item.id;
      list.name = item.name;
    }
    return list;
  }
}
