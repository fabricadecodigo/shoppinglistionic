import { Component, OnInit } from '@angular/core';
import { List } from '../shared/list';
import { ToastService } from './../../core/service/toast.service';
import { AlertService } from 'src/app/core/service/alert.service';
import { ListService } from '../shared/list.service';

@Component({
  selector: 'app-list-list',
  templateUrl: './list-list.page.html',
  styleUrls: ['./list-list.page.scss'],
})
export class ListListPage implements OnInit {
  showSearch: boolean = false;
  lists: List[] = [];

  constructor(private toast: ToastService, private alert: AlertService, private listService: ListService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadLists();
  }

  async loadLists() {
    this.lists = await this.listService.getAll();
  }

  searchButtonClick() {
    this.showSearch = true;
  }

  doSerchBarCancel() {
    this.showSearch = false;
    this.loadLists();
  }

  async doSerchBarChange($event: any) {
    const value = $event.target.value;
    if (value && value.length >= 2) {
      this.lists = await this.listService.filter(value);
    }
  }

  delete(list: List) {
    this.alert.showConfirmDelete(list.name, () => this.executeDelete(list));
  }

  private async executeDelete(list: List) {
    try {
      await this.listService.delete(list);

      const index = this.lists.indexOf(list);
      this.lists.splice(index, 1);
      this.toast.showSuccess('Lista excluída com sucesso.');
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar excluir a Lista.');
    }
  }
}
