import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListListPage } from './list-list.page';

describe('ListListPage', () => {
  let component: ListListPage;
  let fixture: ComponentFixture<ListListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
