import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../core/service/toast.service';
import { ListService } from './../shared/list.service';
import { List } from './../shared/list';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.page.html',
  styleUrls: ['./form-list.page.scss'],
})
export class FormListPage implements OnInit {
  title: string = 'Nova lista';
  list: List;

  constructor(private route: ActivatedRoute, private toast: ToastService, private listService: ListService) { }

  ngOnInit() {
    this.list = new List();

    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      this.title = 'Editar lista';
      this.loadList(parseInt(idParam));
    }
  }

  async loadList(id: number) {
    this.list = await this.listService.getById(id);
  }

  async onSubmit() {
    try {
      const result = await this.listService.save(this.list);
      this.list.id = result.insertId;
      this.toast.showSuccess('Lista salva com sucesso.');
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar salvar a Lista.');
    }
  }
}
