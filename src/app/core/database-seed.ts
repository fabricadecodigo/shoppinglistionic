export class DatabaseSeed {

    static getDatabaseCreateSeed() {
        return this.getCreateTable();
    }

    static getDatabaseSqlSeed() {
        const sqls = [];
        sqls.push(this.getInsertConfigurations());
        sqls.push(this.getInsertCategories());
        sqls.push(this.getInsertLists());
        sqls.push(this.getInsertProducts());
        return sqls.join('\n');
    }
    
    private static getCreateTable() {
        const sqls = [];
        sqls.push('CREATE TABLE IF NOT EXISTS configurations (key varchar(100), value varchar(100));');
        sqls.push('CREATE TABLE IF NOT EXISTS categories (id integer primary key AUTOINCREMENT, name varchar(100));');
        sqls.push('CREATE TABLE IF NOT EXISTS lists (id integer primary key AUTOINCREMENT, name varchar(100));');
        sqls.push('CREATE TABLE IF NOT EXISTS list_items (id integer primary key AUTOINCREMENT, list_id integer, category_id integer, name varchar(100), sequence integer, purchased numeric(1), amount integer);');

        return sqls.join('\n');
    }

    private static getInsertConfigurations() {
        return 'insert into configurations (key, value) values (\'db_version\', \'1\');';
    }

    private static getInsertCategories() {
        const sqls = [];
        sqls.push('insert into categories (id, name) values');
        sqls.push('(1, \'Mercearia\'),');
        sqls.push('(2, \'Carnes, aves e peixes\'),');
        sqls.push('(3, \'Matinais e laticínios\'),');
        sqls.push('(4, \'Hortifruti\'),');
        sqls.push('(5, \'Limpeza e utensilios\'),');
        sqls.push('(6, \'Higiene pessoal\'),');
        sqls.push('(7, \'Congelados\'),');
        sqls.push('(8, \'Doces e sobremesas\'),');
        sqls.push('(9, \'Molho para lanches\');');
        return sqls.join('\n');
    }

    private static getInsertLists() {
        return 'insert into lists (id, name) values (1, \'Compras do mês\');';
    }

    private static getInsertProducts() {
        const sqls = [];
        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values');
        sqls.push('(1, 1, 1, \'Arroz\', 1, 1, 0),');
        sqls.push('(2, 1, 1, \'Feijão vermelho\', 1, 2, 0),');
        sqls.push('(3, 1, 1, \'Feijão carioquinha\', 1, 3, 0),');
        sqls.push('(4, 1, 1, \'Macarrão\', 1, 4, 0),');
        sqls.push('(5, 1, 1, \'Farofa\', 1, 5, 0),');
        sqls.push('(6, 1, 1, \'Farinha de trigo\', 1, 6, 0),');
        sqls.push('(7, 1, 1, \'Molho de tomate\', 1, 7, 0),');
        sqls.push('(8, 1, 1, \'Açúcar\', 1, 8, 0),');
        sqls.push('(9, 1, 1, \'Óleo\', 1, 9, 0),');
        sqls.push('(10, 1, 1, \'Alho\', 1, 10, 0),');
        sqls.push('(11, 1, 1, \'Ervas finas\', 1, 11, 0),');
        sqls.push('(12, 1, 1, \'Queijo parmesão\', 1, 12, 0),');
        sqls.push('(13, 1, 1, \'Batata palha\', 1, 13, 0),');
        sqls.push('(14, 1, 1, \'Palmito\', 1, 14, 0),');
        sqls.push('(15, 1, 1, \'Coador de café\', 1, 15, 0),');
        sqls.push('(16, 1, 1, \'Pó de café\', 1, 16, 0),');
        sqls.push('(17, 1, 1, \'Azeite\', 1, 17, 0),');
        sqls.push('(18, 1, 1, \'Papel alumínio\', 1, 18, 0),');
        sqls.push('(19, 1, 1, \'Papel toalha\', 1, 19, 0),');
        sqls.push('(20, 1, 1, \'Biscoitos\', 1, 20, 0),');
        sqls.push('(21, 1, 1, \'Sal\', 1, 21, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(22, 1, 2, \'Frango\', 3, 1, 0),');
        sqls.push('(23, 1, 2, \'Filé de tilápia\', 1, 2, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(24, 1, 3, \'Requeijão\', 1, 1, 0),');
        sqls.push('(25, 1, 3, \'Iogurte\', 1, 2, 0),');
        sqls.push('(26, 1, 3, \'Margarina\', 1, 3, 0),');
        sqls.push('(27, 1, 3, \'Adoçante\', 1, 4, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(28, 1, 4, \'Banana prata\', 1, 1, 0),');
        sqls.push('(29, 1, 4, \'Pêssego\', 1, 2, 0),');
        sqls.push('(30, 1, 4, \'Abacaxi\', 1, 3, 0),');
        sqls.push('(31, 1, 4, \'Limão\', 1, 4, 0),');
        sqls.push('(32, 1, 4, \'Maçã\', 1, 5, 0),');
        sqls.push('(33, 1, 4, \'Mamão\', 1, 6, 0),');
        sqls.push('(34, 1, 4, \'Pera\', 1, 7, 0),');
        sqls.push('(35, 1, 4, \'Beterraba\', 1, 8, 0),');
        sqls.push('(36, 1, 4, \'Cenoura\', 1, 9, 0),');
        sqls.push('(37, 1, 4, \'Batatas\', 1, 10, 0),');
        sqls.push('(38, 1, 4, \'Batata doce\', 1, 11, 0),');
        sqls.push('(39, 1, 4, \'Tomate\', 1, 12, 0),');
        sqls.push('(40, 1, 4, \'Inhame\', 1, 13, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(41, 1, 5, \'Amaciante\', 1, 1, 0),');
        sqls.push('(42, 1, 5, \'Amaciante concentrado\', 1, 2, 0),');
        sqls.push('(43, 1, 5, \'Veja comum\', 1, 3, 0),');
        sqls.push('(44, 1, 5, \'Veja Gold\', 1, 4, 0),');
        sqls.push('(45, 1, 5, \'Omo\', 1, 5, 0),');
        sqls.push('(46, 1, 5, \'Desinfetante\', 1, 6, 0),');
        sqls.push('(47, 1, 5, \'Ajax\', 1, 7, 0),');
        sqls.push('(48, 1, 5, \'Água sanitária\', 1, 8, 0),');
        sqls.push('(49, 1, 5, \'Buchinha\', 1, 9, 0),');
        sqls.push('(50, 1, 5, \'Sabão em pó\', 1, 10, 0),');
        sqls.push('(51, 1, 5, \'Saco de lixo\', 1, 11, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(52, 1, 6, \'Papel higiênico\', 1, 1, 0),');
        sqls.push('(53, 1, 6, \'Desodorante\', 1, 2, 0),');
        sqls.push('(54, 1, 6, \'Pasta de dente\', 1, 3, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(55, 1, 7, \'Lasanha\', 1, 1, 0),');
        sqls.push('(56, 1, 7, \'Nuguet\', 1, 2, 0),');
        sqls.push('(57, 1, 7, \'Batata congelada\', 1, 3, 0),');
        sqls.push('(58, 1, 7, \'Pão de queijo\', 1, 4, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(59, 1, 8, \'Leite condensado\', 1, 1, 0),');
        sqls.push('(60, 1, 8, \'Creme de leite\', 1, 2, 0);');

        sqls.push('insert into list_items (id, list_id, category_id, name, amount, sequence, purchased) values ');
        sqls.push('(61, 1, 9, \'Maionese\', 1, 1, 0),');
        sqls.push('(62, 1, 9, \'Katchup\', 1, 2, 0),');
        sqls.push('(63, 1, 9, \'3 em 1\', 1, 3, 0)');
        return sqls.join('\n');
    }
}
