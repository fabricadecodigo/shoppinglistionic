import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { DatabaseSeed } from '../database-seed';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  databaseName: string = 'shoppinglist.db';
  db: SQLiteObject

  constructor(private sqlite: SQLite, private sqlitePorter: SQLitePorter) { }

  async openDatabase() {
    try {
      this.db = await this.sqlite.create({ name: this.databaseName, location: 'default' });
      const isDbCreated = await this.createDatabase();
      if (isDbCreated) {
        // import de dados padrão  
        await this.seedDatabase();
      }
    } catch (error) {
      console.error('Ocorreu um erro ao criar o banco de dados', error);
    }
  }

  private async createDatabase() {
    const sqlCreateDatabase = DatabaseSeed.getDatabaseCreateSeed();
    const result = await this.sqlitePorter.importSqlToDb(this.db, sqlCreateDatabase);
    return result ? true : false;
  }

  private async seedDatabase() {
    let currentVersion = 0;
    const result = await this.db.executeSql('select value from configurations where key = ?', ['db_version']);
    if (result && result.rows && result.rows.length > 0) {
      currentVersion = result.rows.item(0).value;
    }

    if (currentVersion == 0) {
      const sqls = DatabaseSeed.getDatabaseSqlSeed();
      return this.sqlitePorter.importSqlToDb(this.db, sqls);
    }
  }

  executeSQL(sql: string, params?: any[]) {
    return this.db.executeSql(sql, params);
  }

  executeSQLWithTransaction(sqlStatements: any[]) {
    return this.db.transaction((tx) => {
      sqlStatements.forEach((sqlStatement: any) => {
        tx.executeSql(sqlStatement.sql, sqlStatement.data);
      });
    });
  }
}
