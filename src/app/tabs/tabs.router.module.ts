import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'lists',
        children: [
          {
            path: '',
            loadChildren: '../lists/list-list/list-list.module#ListListPageModule'
          },
          {
            path: 'new',
            loadChildren: '../lists/form-list/form-list.module#FormListPageModule'
          },
          {
            path: 'edit/:id',
            loadChildren: '../lists/form-list/form-list.module#FormListPageModule'
          },
          {
            path: 'detail/:id',
            loadChildren: '../lists/detail-list/detail-list.module#DetailListPageModule'
          },
          {
            path: 'products',
            children: [
              {
                path: 'new/:listId',
                loadChildren: '../products/form-product/form-product.module#FormProductPageModule'
              },
              {
                path: 'edit/:listId/:id',
                loadChildren: '../products/form-product/form-product.module#FormProductPageModule'
              },
            ]
          }
        ]
      },
      {
        path: 'categories',
        children: [
          {
            path: '',
            loadChildren: '../categories/list-category/list-category.module#ListCategoryPageModule'
          },
          {
            path: 'new',
            loadChildren: '../categories/form-category/form-category.module#FormCategoryPageModule'
          },
          {
            path: 'edit/:id',
            loadChildren: '../categories/form-category/form-category.module#FormCategoryPageModule'
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: '../about/about.module#AboutPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/lists',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/lists',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
