import { Product } from './product';
import { DatabaseService } from './../../core/service/database.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: DatabaseService) { }

  save(product: Product) {
    if (product.id > 0) {
      return this.update(product);
    } else {
      return this.insert(product);
    }
  }

  private insert(product: Product) {
    const sql = 'insert into list_items (list_id, category_id, name, amount, sequence, purchased) values (?, ?, ?, ?, ?, ?)';
    const data = [product.listId, product.categoryId, product.name, product.amount, product.sequence, 0];

    return this.db.executeSQL(sql, data);
  }

  private update(product: Product) {
    const sql = 'update list_items set category_id = ?, name = ?, amount = ? where id = ?';
    const data = [product.categoryId, product.name, product.amount, product.id];

    return this.db.executeSQL(sql, data);
  }

  delete(product: Product) {
    const sql = 'delete from list_items where id = ?';
    const data = [product.id];

    return this.db.executeSQL(sql, data);
  }

  setProductPurchased(id: number, purchased: boolean) {
    const sql = 'update list_items set purchased = ? where id = ?';
    const data = [purchased ? 1 : 0, id];

    return this.db.executeSQL(sql, data);
  }

  async getById(id: number) {
    const sql = 'select * from list_items where id = ?';
    const data = [id];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const product = new Product();
    if (rows && rows.length > 0) {
      // get first item
      const item = rows.item(0);
      product.id = item.id;
      product.listId = item.list_id;
      product.categoryId = item.category_id;
      product.name = item.name;
      product.amount = item.amount;
      product.sequence = item.sequence;
      product.purchased = (item.purchased == 1);
    }
    return product;
  }

  async getNewSequence(listId: number, categoryId: number) {
    const sql = 'select COUNT(Id) as amount from list_items where list_id = ? and category_id = ?';
    const data = [listId, categoryId];

    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    let amount: number = 0;

    if (rows && rows.length > 0) {
      const item = rows.item(0);
      amount = item.amount;
    }

    amount++;
    return amount;
  }

  getSearchQuery(hasTextFilter: boolean = false) {
    const sqlBuilder = [];
    sqlBuilder.push('select');
    sqlBuilder.push('c.id categoryId,');
    sqlBuilder.push('c.name categoryName,');
    sqlBuilder.push('i.id,');
    sqlBuilder.push('i.name,');
    sqlBuilder.push('i.amount,');
    sqlBuilder.push('i.sequence,');
    sqlBuilder.push('i.purchased');
    sqlBuilder.push('from');
    sqlBuilder.push('list_items i inner join lists l on i.list_id = l.id inner join categories c on i.category_id = c.id');
    sqlBuilder.push('where');
    sqlBuilder.push('i.list_id = ?');
    if (hasTextFilter) {
      sqlBuilder.push('and i.name like ?');
    }    
    sqlBuilder.push('order by c.name, i.sequence');

    return sqlBuilder.join(' ');
  }

  private fillProducts(rows: any) {
    const products: any[] = [];

    for (let i = 0; i < rows.length; i++) {
      const item = rows.item(i);
      const productByCategory: any = {};
      productByCategory.categoryId = item.categoryId;
      productByCategory.categoryName = item.categoryName;
      productByCategory.id = item.id;
      productByCategory.name = item.name;
      productByCategory.amount = item.amount;
      productByCategory.sequence = item.sequence;
      productByCategory.purchased = (item.purchased == 1);
      
      products.push(productByCategory);
    }

    return products;
  }

  async getAll(listId: number) {
    const sql = this.getSearchQuery();
    const data = [listId];
    const result = await this.db.executeSQL(sql, data);
    const products = this.fillProducts(result.rows);
    return products;
  }

  async filter(listId: number, text: string) {
    const sql = this.getSearchQuery(true);    
    const data = [listId, `%${text}%`];
    const result = await this.db.executeSQL(sql, data);
    const list = this.fillProducts(result.rows);
    return list;
  }

  updateProductsSequence(products: any[]) {
    const sqlStatements: any[] = [];
   
    products.forEach((product: any) => {
      const sql = 'update list_items set sequence = ? where id = ?';
      const data = [product.sequence, product.id];
      sqlStatements.push({ sql: sql, data: data });
    });
   
    return this.db.executeSQLWithTransaction(sqlStatements);
  }
}
