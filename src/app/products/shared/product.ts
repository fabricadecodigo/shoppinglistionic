export class Product {
    id: number;
    listId: number;
    categoryId: number;
    name: string;
    amount: number;
    sequence: number;
    purchased: boolean;
}
