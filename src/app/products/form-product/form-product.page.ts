import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../core/service/toast.service';
import { CategoryService } from './../../categories/shared/category.service';
import { ProductService } from './../shared/product.service';
import { Product } from '../shared/product';
import { Category } from './../../categories/shared/category';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.page.html',
  styleUrls: ['./form-product.page.scss'],
})
export class FormProductPage implements OnInit {
  title: string = 'Novo produto';
  product: Product;
  categories: Category[] = [];
  listId: number = -1;

  constructor(private route: ActivatedRoute, private toast: ToastService, 
    private productService: ProductService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.product = new Product();
    
    const listIdParam = this.route.snapshot.paramMap.get('listId');    
    const idParam = this.route.snapshot.paramMap.get('id');

    if (listIdParam) {
      this.listId = parseInt(listIdParam);
    }

    this.loadCategories();
    if (idParam) {
      this.title = 'Editar produto';
      this.loadProduct(parseInt(idParam));
    }
  }

  async loadProduct(id: number) {
    this.product = await this.productService.getById(id);
  }

  async loadCategories() {
    this.categories = await this.categoryService.getAll();
  }

  async onSubmit() {
    try {
      await this.fillComplementaryData();

      const result = await this.productService.save(this.product);
      this.product.id = result.insertId;      
      this.toast.showSuccess('Produto salvo com sucesso.');
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar salvar o Produto.');
    }
  }

  async fillComplementaryData() {
    if (!this.product.id) {
      this.product.listId = this.listId;
      this.product.sequence = await this.productService.getNewSequence(this.listId, this.product.categoryId);
    }
  }
}
