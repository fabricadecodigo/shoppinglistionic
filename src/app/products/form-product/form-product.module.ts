import { MinValueDirective } from './../../core/validation/min-value.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FormProductPage } from './form-product.page';

const routes: Routes = [
  {
    path: '',
    component: FormProductPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FormProductPage, MinValueDirective]
})
export class FormProductPageModule {}
