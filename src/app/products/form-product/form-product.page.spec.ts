import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProductPage } from './form-product.page';

describe('FormProductPage', () => {
  let component: FormProductPage;
  let fixture: ComponentFixture<FormProductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProductPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
