import { DatabaseService } from './../../core/service/database.service';
import { Injectable } from '@angular/core';
import { Category } from './category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private db: DatabaseService) { }

  save(category: Category) {
    if (category.id) {
      return this.update(category);
    } else {
      return this.insert(category);
    }
  }

  insert(category: Category) {
    const sql = 'insert into categories (name) values (?)'
    const data = [category.name];

    return this.db.executeSQL(sql, data);
  }

  update(category: Category) {
    const sql = 'update categories set name = ? where id = ?'
    const data = [category.name, category.id];

    return this.db.executeSQL(sql, data);
  }

  delete(category: Category) {
    const sql = 'delete from categories where id = ?'
    const data = [category.id];

    return this.db.executeSQL(sql, data);
  }
  
  fillCategories(rows: any) {
    const categories: Category[] = [];

    for (let i = 0; i < rows.length; i++) {
      const item = rows.item(i);
      const category = new Category();
      category.id = item.id;
      category.name = item.name;
      categories.push(category);
    }

    return categories;
  }

  async getAll() {
    const sql = 'select * from categories';
    const result = await this.db.executeSQL(sql);
    const categories = this.fillCategories(result.rows);
    return categories;
  }

  async filter(text: string) {
    const sql = 'select * from categories where name like ?';
    const data = [`%${text}%`];
    const result = await this.db.executeSQL(sql, data);
    const categories = this.fillCategories(result.rows);
    return categories;
  }

  async getById(id: number) {
    const sql = 'select * from categories where id = ?';
    const data = [id];
    const result = await this.db.executeSQL(sql, data);
    const rows = result.rows;
    const category = new Category();
    if (rows && rows.length > 0) {
      const item = rows.item(0);
      category.id = item.id;
      category.name = item.name;
    }

    return category;
  }
}
