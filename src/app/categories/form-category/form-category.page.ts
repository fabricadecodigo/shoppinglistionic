import { Category } from './../shared/category';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from './../shared/category.service';
import { ToastService } from '../../core/service/toast.service';

@Component({
  selector: 'app-form-category',
  templateUrl: './form-category.page.html',
  styleUrls: ['./form-category.page.scss'],
})
export class FormCategoryPage implements OnInit {
  title: string = 'Nova categoria';
  category: Category;

  constructor(private route: ActivatedRoute, private toast: ToastService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.category = new Category();

    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      this.title = 'Editar categoria';
      this.loadCategory(parseInt(idParam));
    }
  }

  async loadCategory(id: number) {
    this.category = await this.categoryService.getById(id);
  }

  async onSubmit() {
    try {
      const result = await this.categoryService.save(this.category);
      this.category.id = result.insertId;
      this.toast.showSuccess('Categoria salva com sucesso.');        
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar salvar a Categoria.');
    }
  }
}
