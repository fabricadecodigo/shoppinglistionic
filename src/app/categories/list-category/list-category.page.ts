import { Component, OnInit } from '@angular/core';
import { Category } from '../shared/category';
import { ToastService } from './../../core/service/toast.service';
import { AlertService } from './../../core/service/alert.service';
import { CategoryService } from '../shared/category.service';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.page.html',
  styleUrls: ['./list-category.page.scss'],
})
export class ListCategoryPage implements OnInit {
  showSearch: boolean = false;
  categories: Category[] = [];

  constructor(private toast: ToastService, private alert: AlertService, private categoryService: CategoryService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadCategories();
  }

  async loadCategories() {
    this.categories = await this.categoryService.getAll();
  }

  searchButtonClick() {
    this.showSearch = true;
  }

  doSerchBarCancel() {
    this.showSearch = false;
    this.loadCategories();
  }

  async doSerchBarChange($event: any) {
    const value = $event.target.value;
    if (value && value.length >= 2) {
      this.categories = await this.categoryService.filter(value);
    }
  }

  delete(category: Category) {
    this.alert.showConfirmDelete(category.name, () => this.executeDelete(category));
  }

  private async executeDelete(category: Category) {
    try {
      await this.categoryService.delete(category);

      const index = this.categories.indexOf(category);
      this.categories.splice(index, 1);
      this.toast.showSuccess('Categoria excluída com sucesso.');
    } catch (error) {
      this.toast.showError('Ocorreu um erro ao tentar excluir a Categoria.');
    }
  }
}
